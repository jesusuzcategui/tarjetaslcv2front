import axios from 'axios';
import { APISERVICE } from '../enviroment';

async function initTransaction(){
    let resp = null;

    const { data, status} = await axios.get(`${APISERVICE}webpay/createPayment`);
    if(status === 200){
        resp = data.data;
    } else {
        resp = null;
    }

    return resp;
}

async function goToWebpay(ammout, ammountCode, email, phone){
    let resp = null;
    let form = new FormData();
    form.append('email', email);
    form.append('ammout', ammout);
    form.append('ammoutCode', ammountCode);
    form.append('phone', phone);

    const { data, status } = await axios.post(`${APISERVICE}webpay/initTransaction`, form);

    if( status === 200 ){
        resp = data;
    } else {
        resp = null;
    }

    return resp;
}

export { initTransaction, goToWebpay };