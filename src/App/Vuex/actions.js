import { initTransaction } from "../../services/webpay";
import { findCardAviables } from "../../services/tarjetas";
import card1000 from '../../assets/card/1000.png';
import card2000 from '../../assets/card/2000.png';
import card5000 from '../../assets/card/5000.png';
import card10000 from '../../assets/card/10000.png';

const setWebpayAction = ( context ) => {
    initTransaction().then(resp => {context.commit('setWebpay', resp)});
};

const setTarjetasAction = ( context ) => {
    findCardAviables().then(resp => {
        const newsCard = resp.map(card => {
            return card.idprecio === 1 ? {...card, image: card1000, title: 'Tarjeta de 1000'} : card.idprecio === 2 ? {...card, image: card2000, title: 'Tarjeta de 2000'} : card.idprecio === 3 ? {...card, image: card5000, title: 'Tarjeta de 5000'} : card.idprecio === 6 ? {...card, image: card10000, title: 'Tarjeta de 10000'} : card;
        });
        context.commit('setTarjetas', newsCard);
    });
};

const setCartAction = ( context, payload ) => {
    context.commit('setCart', payload);
};

export { setWebpayAction, setTarjetasAction, setCartAction };