const setWebpay = (state, data) => {
    state.webpay = data;
};

const setTarjetas = (state, data) => {
    state.tarjetas = data;
};

const setCart = (state, data) => {
    state.cart = data;
};

export { setWebpay, setTarjetas, setCart };