import Vue from 'vue';
import Vuex from 'vuex';
import { setWebpay, setTarjetas, setCart } from './App/Vuex/mutation';
import { setWebpayAction, setTarjetasAction, setCartAction } from './App/Vuex/actions';
Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        webpay: null,
        tarjetas: [],
        cart: {idprice: null, price: null, email: null, phone: null},
    },
    mutations: {
        setWebpay,
        setTarjetas,
        setCart
    },
    actions: {
        setWebpayAction,
        setTarjetasAction,
        setCartAction
    },
    getters: {
        getWebpay(state){
            return state.webpay;
        },
        getTarjetas( state ){
            return state.tarjetas;
        },
        getCart( state ){
            return state.cart;
        },
        getTarjeta(state){
            return (id) => {
                return state.tarjetas.find(card => card.idprecio === id);
            }
        }
    }
});