import VueRouter from 'vue-router';

import IndexPage from './App/Pages/Index.vue';
import CartPage from './App/Pages/Cart.vue';
import WebapyPage from './App/Pages/Webpay.vue';
import ProccessPage from './App/Pages/Process.vue';

const routes = [
    {
        path: "/",
        component: IndexPage,
        name: "Home"
    },
    {
        path: "/cart",
        component: CartPage,
        name: "Cart"
    },
    {
        path: "/webpay",
        component: WebapyPage,
        name: "Webpay"
    },
    {
        path: "/validate",
        component: ProccessPage,
        name: "Procesando"
    }
];

const router = new VueRouter({
    mode: "history",
    routes: routes
});

export default router;